from flask import Flask, render_template
from togglwrapper import Toggl
import json

app = Flask(__name__)

@app.route("/")
def index():
    toggl = Toggl('token')
    t_entries = toggl.TimeEntries.get(start_date='2017-09-10T00:00:00+00:00', end_date='2017-10-13T23:59:59+00:00')

    colors = {
        'Forritun': 'red',
        'Starfsmannafélag': 'blue',
        'IT': 'green',
        'Verkefnastjórnun': 'brown',
        'No tag': 'grey',
    }

    return_list = []
    for i in t_entries:
        curr_dict = {}
        curr_dict['start'] = i.get('start','')[:-9]
        curr_dict['end'] = i.get('stop','')[:-9]
        curr_dict['title'] = i.get('description','')
        curr_dict['color'] = colors.get(i.get('tags',['No tag'])[0], 'grey')
        return_list.append(curr_dict)

    return render_template('templates/index.html', events=json.dumps(return_list))